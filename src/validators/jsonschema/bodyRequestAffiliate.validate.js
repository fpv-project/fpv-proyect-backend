const bodyRequestAffiliate = {
    id: "bodyRequestAffiliate", 
    type: "object",
    properties: {
        fpvCatalog: {
            type: "string", 
            pattern: "^[a-z0-9]",
            minLength: 32,
            maxLength: 32
        }, 
        fpvAliasName:{
            type: "string", 
            pattern: "^[ a-zA-Z0-9_-]",
            minLength: 4,
            maxLength: 20
        },
        amount: {
            type: "number", 
        }
    },
    required: ["fpvCatalog", "fpvAliasName","amount"]
}

module.exports = { bodyRequestAffiliate };
