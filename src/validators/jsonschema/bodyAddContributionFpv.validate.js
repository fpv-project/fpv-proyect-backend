const bodyAddContributionFpv = {
    id: "bodyAddContributionFpv", 
    type: "object",
    properties: {
        fpvFound: {
            type: "string", 
            pattern: "^[a-z0-9]",
            minLength: 32,
            maxLength: 32
        }, 
        reference: {
            type: "string", 
            pattern: "^[ a-z0-9]",
            minLength: 10,
            maxLength: 50
        },
        amount: {
            type: "number", 
            minimum: 50000
        }
    },
    required: ["fpvFound", "amount", "reference"]
}

module.exports = { bodyAddContributionFpv };