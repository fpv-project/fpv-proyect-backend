const bodyCancelFpv = {
    id: "bodyCancelFpv", 
    type: "object",
    properties: {
        fpvFound: {
            type: "string", 
            pattern: "^[a-z0-9]",
            minLength: 32,
            maxLength: 32
        }
    },
    required: ["fpvFound"]
}

module.exports = { bodyCancelFpv };