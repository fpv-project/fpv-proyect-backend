const bodyGetFpvByIdFpvUniqueIdUser = {
    id: "bodyGetFpvByIdFpvUniqueIdUser", 
    type: "object",
    properties: {
        fpvFound: {
            type: "string", 
            pattern: "^[a-z0-9]",
            minLength: 32,
            maxLength: 32
        }
    },
    required: ["fpvFound"]
}

module.exports = { bodyGetFpvByIdFpvUniqueIdUser };