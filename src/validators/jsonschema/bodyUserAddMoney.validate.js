const bodyUserAddMoney = {
    id: "bodyUserAddMoney", 
    type: "object",
    properties: {
        reference: {
            type: "string", 
            pattern: "^[ a-z0-9]",
            minLength: 10,
            maxLength: 50
        },
        amount: {
            type: "number", 
            minimum: 50000,
            maximum: 1000000
        }
    },
    required: [ "amount", "reference"]
}

module.exports = { bodyUserAddMoney };