const { timeStamp } = require("../utils/general/timeStamp.util");
const { getCatalogs } = require("../data/getCatalogs.data");
const { msn } = require("../const/msn.const");
const { ResponseBusiness } = require("../business/response.business");
const { sendFvpError } = require("../sqs/sqsFvpError.sqs");

module.exports.app = async (event, context, _callback) => {
    let response = null;
    try {
        let items = [];
        const fpvCatalogs = await getCatalogs();

        fpvCatalogs.Items.forEach( data => {
            items.push(
                {
                    fvpCatalog: data.idFvpCatalog,
                    maximum: data.maximum,
                    category: data.category,
                    minumum: data.minumum,
                    name: data.name
                }
            );
        });
        

        response = new ResponseBusiness(200, msn.SUCCESS, items);

    } catch (error) {
        console.log(error)
        const errorData = {
            validation: !!error.validation ? error.validation : null,
            statusCode: !!error.statusCode ? error.statusCode : 500,
            statusMessage: !!error.statusMessage ? error.statusMessage : msn.INTERNAL,
            time: !!error.time ? error.time : timeStamp()
        }

        response = new ResponseBusiness(errorData.statusCode, errorData.statusMessage, errorData.validation);

        try {
            const sendToSqs = {
                context,
                event,
                errorData,
                errorMessage: error.message,
                errorName: error.name,
                type: "ERROR_IN_ADD_FOUNDS_USER"
            };

            sendFvpError(JSON.stringify(sendToSqs));
        } catch (err) {
            console.log("Error in called sq");
        }

    }

    return response.getResponse();
}