const { timeStamp } = require("../utils/general/timeStamp.util");
const { FpvBusiness } = require("../business/fpv.business");
const { bodyGetFpvByIdFpvUniqueIdUser } = require("../validators/jsonschema/bodyGetFpvByIdFpvUniqueIdUser.validate");
const { msn } = require("../const/msn.const");
const { ResponseBusiness } = require("../business/response.business");
const { sendFvpError } = require("../sqs/sqsFvpError.sqs");

module.exports.app = async (event, context, _callback) => {
    let body = null;
    let response = null;

    try {
        const typeRequest = process.env.typeRequest;
        const tableName   = process.env.tableBankName;
        const uniqueIdUser = "eqewqweq121212";
    
        const newFpv = new FpvBusiness(typeRequest,event, tableName);
        newFpv.setContextPetition();
        
        body = newFpv.geBody();
        
        newFpv.validateBodyRequest(body,bodyGetFpvByIdFpvUniqueIdUser, msn.VLDREQUEST);

        const dataFpVRegistrer = await newFpv.getDataIdFvpUserInformation(body.fpvFound,uniqueIdUser);

        const contribution = dataFpVRegistrer.contribution;
        
        dataFpVRegistrer.contribution = contribution.reverse();
    
        response = new ResponseBusiness(200, msn.SUCCESS, dataFpVRegistrer);
    
      } catch (error) {
        console.log(error)
        const errorData = {
          validation: !!error.validation ? error.validation: null,
          statusCode: !!error.statusCode ? error.statusCode : 500,
          statusMessage: !!error.statusMessage ? error.statusMessage: msn.INTERNAL,
          time: !!error.time ? error.time  : timeStamp()
        }
    
        response = new ResponseBusiness(errorData.statusCode, errorData.statusMessage, errorData.validation);
    
        try{
          const sendToSqs = {
            context,
            event, 
            errorData,
            errorMessage: error.message,
            errorName: error.name,
            type: "ERROR_IN_AFFILIATE_FPV"
          };
    
          sendFvpError(JSON.stringify(sendToSqs));
        } catch (err){
          console.log("Error in called sq");
        }
    
      }
        
      return response.getResponse();

}    