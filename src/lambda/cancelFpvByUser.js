const { timeStamp } = require("../utils/general/timeStamp.util");
const { AddFoundsFpv } = require("../business/addFoundsFpv.business");
const { bodyCancelFpv } = require("../validators/jsonschema/bodyCancelFpv.validate");
const { msn } = require("../const/msn.const");
const { ResponseBusiness } = require("../business/response.business");
const { sendFvpError } = require("../sqs/sqsFvpError.sqs");

module.exports.app = async (event, context, _callback) => {
    let body = null;
    let response = null;
    try {
        const typeRequest = process.env.typeRequest;
        const tableName = process.env.tableBankName;
        const uniqueIdUser = "eqewqweq121212";

        const newFpvContribution = new AddFoundsFpv(typeRequest,event, tableName);
        newFpvContribution.setContextPetition();
        body = newFpvContribution.geBody();

        newFpvContribution.validateBodyRequest(body,bodyCancelFpv, msn.VLDREQUEST);
        const dataFpVRegistrer = await newFpvContribution.getDataIdFvpUser(body.fpvFound,uniqueIdUser);
        
        const userData = await newFpvContribution.getCurrencyAmountByUser(uniqueIdUser);
        const indexFpv = dataFpVRegistrer.count;
        const idFpv = dataFpVRegistrer.idFvp;
        const referenceFpv = `Cancelacion de fondo ${dataFpVRegistrer.name}`;
        const contributionAmount = 0;

        await newFpvContribution.addFvpContribution(-dataFpVRegistrer.currencyAmount,indexFpv, idFpv, uniqueIdUser,referenceFpv,contributionAmount);
        await newFpvContribution.cancelFpvByIdFpvAndIdUser(idFpv,uniqueIdUser);
        
        const index = userData.count;
        const idUser = userData.idUser;
        const reference = `Cancelacion de fondo ${dataFpVRegistrer.name}`;
        const registerAmount = userData.currencyAmount + dataFpVRegistrer.currencyAmount;
        
        await newFpvContribution.putMovimentAffiliate(dataFpVRegistrer.currencyAmount, index, idUser, uniqueIdUser, reference, registerAmount);  
        
        response = new ResponseBusiness(200, msn.SUCCESS, true);

    } catch (error) {
        const errorData = {
            validation: !!error.validation ? error.validation : null,
            statusCode: !!error.statusCode ? error.statusCode : 500,
            statusMessage: !!error.statusMessage ? error.statusMessage : msn.INTERNAL,
            time: !!error.time ? error.time : timeStamp()
        }

        response = new ResponseBusiness(errorData.statusCode, errorData.statusMessage, errorData.validation);

        try {
            const sendToSqs = {
                context,
                event,
                errorData,
                errorMessage: error.message,
                errorName: error.name,
                type: "ERROR_CANCEL_FPV_USER"
            };

            sendFvpError(JSON.stringify(sendToSqs));
        } catch (err) {
            console.log("Error in called sq");
        }

    }

    return response.getResponse();
}