const { timeStamp } = require("../utils/general/timeStamp.util");
const { AffiliateBusiness } = require("../business/affiliate.bussiness");
const { bodyRequestAffiliate } = require("../validators/jsonschema/bodyRequestAffiliate.validate");
const { msn } = require("../const/msn.const");
const { ResponseBusiness } = require("../business/response.business");
const { sendFvpError } = require("../sqs/sqsFvpError.sqs");

module.exports.app = async (event, context, _callback) => {
  let body = null;
  let response = null;
  try {
    const typeRequest = process.env.typeRequest;
    const tableName   = process.env.tableBankName;
    const uniqueIdUser = "eqewqweq121212";

    const newFpv = new AffiliateBusiness(typeRequest,event, tableName);

    newFpv.setContextPetition();
    
    body = newFpv.geBody();

    newFpv.validateBodyRequest(body,bodyRequestAffiliate, msn.VLDREQUEST);

    const catalog = await newFpv.getCatalogoByIdFvpCatalog(body.fpvCatalog);

    await newFpv.validateIfUserOwnFpvType(catalog.name,uniqueIdUser);

    newFpv.validateAmountOpen(catalog, body.amount);

    const userData = await newFpv.getCurrencyAmountByUser(uniqueIdUser);

    newFpv.validateCurrencyAmountVsAmount(userData.currencyAmountMoney,body.amount);

    const index = userData.count;
    const idUser = userData.idUser;
    const reference = `${msn.APORTEMSN} ${catalog.name}`;
    const registerAmount = userData.currencyAmount - body.amount;
    
    await newFpv.putMovimentAffiliate(-body.amount, index, idUser, uniqueIdUser, reference, registerAmount);

    const idFpv = await newFpv.registerContributionFound(catalog.name, body.fpvAliasName, catalog.category, 1, body.amount);

    response = new ResponseBusiness(200, msn.SUCCESS, idFpv);

  } catch (error) {
    const errorData = {
      validation: !!error.validation ? error.validation: null,
      statusCode: !!error.statusCode ? error.statusCode : 500,
      statusMessage: !!error.statusMessage ? error.statusMessage: msn.INTERNAL,
      time: !!error.time ? error.time  : timeStamp()
    }

    response = new ResponseBusiness(errorData.statusCode, errorData.statusMessage, errorData.validation);

    try{
      const sendToSqs = {
        context,
        event, 
        errorData,
        errorMessage: error.message,
        errorName: error.name,
        type: "ERROR_IN_AFFILIATE_FPV"
      };

      sendFvpError(JSON.stringify(sendToSqs));
    } catch (err){
      console.log("Error in called sq");
    }

  }
    
  return response.getResponse();
    
};


