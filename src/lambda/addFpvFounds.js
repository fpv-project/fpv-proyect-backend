const { timeStamp } = require("../utils/general/timeStamp.util");
const { AddFoundsFpv } = require("../business/addFoundsFpv.business");
const { bodyAddContributionFpv } = require("../validators/jsonschema/bodyAddContributionFpv.validate");
const { msn } = require("../const/msn.const");
const { ResponseBusiness } = require("../business/response.business");
const { sendFvpError } = require("../sqs/sqsFvpError.sqs");

module.exports.app = async (event, context, _callback) => {
    let body = null;
    let response = null;
    try {
      const typeRequest = process.env.typeRequest;
      const tableName   = process.env.tableBankName;
      const uniqueIdUser = "eqewqweq121212";
      const newFpvContribution = new AddFoundsFpv(typeRequest,event, tableName);

      newFpvContribution.setContextPetition();
      body = newFpvContribution.geBody();

      newFpvContribution.validateBodyRequest(body,bodyAddContributionFpv, msn.VLDREQUEST);

      const dataFpVRegistrer = await newFpvContribution.getDataIdFvpUser(body.fpvFound,uniqueIdUser);

      const userData = await newFpvContribution.getCurrencyAmountByUser(uniqueIdUser);
      newFpvContribution.validateCurrencyAmountVsAmount(userData.currencyAmountMoney,body.amount);

      const indexFpv = dataFpVRegistrer.count;
      const idFpv = dataFpVRegistrer.idFvp;
      const referenceFpv = `${body.reference}`;
      const contributionAmount = dataFpVRegistrer.currencyAmount + body.amount;

      await newFpvContribution.addFvpContribution(body.amount,indexFpv, idFpv, uniqueIdUser,referenceFpv,contributionAmount);
      
      const index = userData.count;
      const idUser = userData.idUser;
      const reference = `${msn.APORTEMSN} ${dataFpVRegistrer.name}`;
      const registerAmount = userData.currencyAmount - body.amount;

      await newFpvContribution.putMovimentAffiliate(-body.amount, index, idUser, uniqueIdUser, reference, registerAmount);  

      response = new ResponseBusiness(200, msn.SUCCESS, true);
  
    } catch (error) {
        console.log(error)
      const errorData = {
        validation: !!error.validation ? error.validation: null,
        statusCode: !!error.statusCode ? error.statusCode : 500,
        statusMessage: !!error.statusMessage ? error.statusMessage: msn.INTERNAL,
        time: !!error.time ? error.time  : timeStamp()
      }
  
      response = new ResponseBusiness(errorData.statusCode, errorData.statusMessage, errorData.validation);
  
      try{
        const sendToSqs = {
          context,
          event, 
          errorData,
          errorMessage: error.message,
          errorName: error.name,
          type: "ERROR_IN_ADD_FPV_FOUNDS_FPV"
        };
  
        sendFvpError(JSON.stringify(sendToSqs));
      } catch (err){
        console.log("Error in called sq");
      }
  
    }
      
    return response.getResponse();
}