const { timeStamp } = require("../utils/general/timeStamp.util");
const { Users } = require("../business/users.business");
const { msn } = require("../const/msn.const");
const { ResponseBusiness } = require("../business/response.business");
const { sendFvpError } = require("../sqs/sqsFvpError.sqs");

module.exports.app = async (event, context, _callback) => {
  let body = null;
  let response = null;
  try {
    const typeRequest = process.env.typeRequest;
    const uniqueIdUser = "eqewqweq121212";
    const userBusiness = new Users(typeRequest,event);
    const userData = await userBusiness.getUserData(uniqueIdUser);

    const moviments = userData.moviment.sort((a,b) => a.timeRegesiter - b.timeRegesiter  );
    
    userData.moviment = moviments.reverse();

    response = new ResponseBusiness(200, msn.SUCCESS, userData);

  } catch (error) {
    const errorData = {
      validation: !!error.validation ? error.validation: null,
      statusCode: !!error.statusCode ? error.statusCode : 500,
      statusMessage: !!error.statusMessage ? error.statusMessage: msn.INTERNAL,
      time: !!error.time ? error.time  : timeStamp()
    }

    response = new ResponseBusiness(errorData.statusCode, errorData.statusMessage, errorData.validation);

    try{
      const sendToSqs = {
        context,
        event, 
        errorData,
        errorMessage: error.message,
        errorName: error.name,
        type: "ERROR_IN_GET_USER_DATA"
      };

      sendFvpError(JSON.stringify(sendToSqs));
    } catch (err){
      console.log("Error in called sq");
    }

  }
    
  return response.getResponse();
}


