const { Context } = require("../patterns/strategy/bodyRequest/context/contextManage.context");
const { JsonSchemaValidator } = require("../validators/validateOrError/jsonSchemaValidator.validate");
const { cancelFpvByIdFpvAndIdUser } = require("../data/cancelFpvByIdFpvAndIdUser.data");
const { timeStamp } = require("../utils/general/timeStamp.util");
const { queryItems } = require("../data/queryItems.data");
const { msn } = require("../const/msn.const");

exports.FpvBusiness = class FpvBusiness{
    constructor(typeRequest, event, tableName){
        this._typeRequest = typeRequest;
        this._event = event; 
        this._fvpTable = tableName;
    }

    setContextPetition(){
        this._context = new Context(this._typeRequest, this._event);
        this._context.setStrategy();
    }

    geBody(){
        this._context.chooseStrategy();
        return this._context.getAction();
    }

    validateBodyRequest(data, schema, msnError){
        JsonSchemaValidator(data, schema, msnError);
    }

    async getCatalogoByIdFvpCatalog(idFvpCatalog){
        const params = {
            idFvpCatalog,
            status: 1
        };
        const catalogoData = await queryItems(params,process.env.tableFpvCatalog);

        if(catalogoData.Count === 0){
            const error = new Error(msn.CATGNOTFOUND);
            error.validation = null;
            error.statusCode = 404;
            error.statusMessage = msn.CATGNOTFOUND;
            error.time = timeStamp();
            throw error;
        } 
        return catalogoData.Items[0];
    } 

    async cancelFpvByIdFpvAndIdUser(idFvp,uniqueIdUser){
        await cancelFpvByIdFpvAndIdUser(idFvp,uniqueIdUser)
    }

    async getCurrencyAmountByUser(uniqueIdUser){
        const params = {
            uniqueIdUser,
            status: 1
        };
        const currencyAmount = await queryItems(params,process.env.tableFounds);

        if(currencyAmount.Count === 0){
            const error = new Error(msn.USERNOTFOUND);
            error.validation = null;
            error.statusCode = 404;
            error.statusMessage = msn.USERNOTFOUND;
            error.time = timeStamp();
            throw error;
        } 

        let currencyAmountMoney = 0;

        currencyAmount.Items[0].moviment.forEach( data => {
            currencyAmountMoney += data.amount;
        })

        return {
            currencyAmountMoney, 
            count: currencyAmount.Items[0].moviment.length,
            idUser: currencyAmount.Items[0].idUser,
            currencyAmount: currencyAmount.Items[0].currencyAmount
        };
    }

    validateCurrencyAmountVsAmount(currencyAmount, amount){
        if(amount > currencyAmount){
            const error = new Error(msn.PENNILESS);
            error.validation = null;
            error.statusCode = 404;
            error.statusMessage = msn.PENNILESS;
            error.time = timeStamp();
            throw error;
        } 
    }

    async validateIfUserOwnFpvType(name,uniqueIdUser){
        const params = {
            uniqueIdUser,
            name,
            status: 1
        };
        const catalogoData = await queryItems(params,process.env.tableBankName);

        if(catalogoData.Count > 0){
            const error = new Error(msn.EXITINFPV);
            error.validation = null;
            error.statusCode = 500;
            error.statusMessage = msn.EXITINFPV;
            error.time = timeStamp();
            throw error;
        } 
        return catalogoData.Items[0];
    } 

    async validateIfUserIsOwnFpvType(name,uniqueIdUser){
        const params = {
            uniqueIdUser,
            name,
            status: 1
        };
        const currencyAmount = await queryItems(params,process.env.tableBankName);

        if(currencyAmount.Count < 1){
            const error = new Error(msn.NOTISOWN);
            error.validation = null;
            error.statusCode = 500;
            error.statusMessage = msn.NOTISOWN;
            error.time = timeStamp();
            throw error;
        } 

        let currencyAmountMoney = 0;

        currencyAmount.Items[0].contribution.forEach( data => {
            currencyAmountMoney += data.amount;
        })

        return {
            currencyAmountMoney, 
            count: currencyAmount.Items[0].contribution.length,
            idFvp: currencyAmount.Items[0].idFvp,
            currencyAmount: currencyAmount.Items[0].currencyAmount
        };
    } 

    async getDataIdFvpUser(idFvp,uniqueIdUser){
        const params = {
            idFvp,
            uniqueIdUser,
            status: 1
        };
        const currencyAmount = await queryItems(params,process.env.tableBankName);

        if(currencyAmount.Count < 1){
            const error = new Error(msn.INACTIVEFPV); 
            error.validation = null;
            error.statusCode = 404;
            error.statusMessage = msn.INACTIVEFPV;
            error.time = timeStamp();
            throw error;
        } 
        let currencyAmountMoney = 0;

        currencyAmount.Items[0].contribution.forEach( data => {
            currencyAmountMoney += data.amount;
        })

        return {
            currencyAmountMoney, 
            count: currencyAmount.Items[0].contribution.length,
            idFvp: currencyAmount.Items[0].idFvp,
            name: currencyAmount.Items[0].name,
            currencyAmount: currencyAmount.Items[0].currencyAmount
        };
    }

    async getDataIdFvpUserInformation(idFvp,uniqueIdUser){
        const params = {
            idFvp,
            uniqueIdUser,
            status: 1
        };
        const items = [];
        const currencyAmount = await queryItems(params,process.env.tableBankName);

        if(currencyAmount.Count < 1){
            const error = new Error(msn.INACTIVEFPV); 
            error.validation = null;
            error.statusCode = 404;
            error.statusMessage = msn.INACTIVEFPV;
            error.time = timeStamp();
            throw error;
        } 

        let currencyAmountMoney = 0;

        currencyAmount.Items[0].contribution.forEach( data => {
            currencyAmountMoney += data.amount;
            console.log(data)
            items.push(
                {
                    register: data.idContributionFvp,
                    amount: data.amount,
                    reference: data.reference,
                    timeRegesiter: new Date(data.timeRegesiter).toISOString()
                }
            );
        })

        return {
            currencyAmountMoney, 
            count: currencyAmount.Items[0].contribution.length,
            contribution: items,
            idFvp: currencyAmount.Items[0].idFvp,
            name: currencyAmount.Items[0].name,
            currencyAmount: currencyAmount.Items[0].currencyAmount
        };
    }
}