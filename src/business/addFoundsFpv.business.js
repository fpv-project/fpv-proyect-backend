const { FpvBusiness } = require("./fpv.business");
const { id }  = require("../utils/dynamo/idGenerator.utils");
const { timeStamp } = require("../utils/general/timeStamp.util");
const { putFpvContribution } = require("../data/putContribituion.data");
const { putMoviment } = require("../data/putMoviment.data");
const { queryItems } = require("../data/queryItems.data");
const { msn } = require("../const/msn.const");

exports.AddFoundsFpv = class AddFoundsFpv extends FpvBusiness{
    constructor(typeRequest, event, tableName){
        super(typeRequest, event, tableName);
    }

    async addFvpContribution(amount, index, idFvp, uniqueIdUser, reference, registerAmount){
        await putFpvContribution(amount, index, idFvp, uniqueIdUser, reference, registerAmount);
    }

    
    async putMovimentAffiliate(amount, index, idUser, uniqueIdUser, reference, registerAmount){
        return putMoviment(amount, index, idUser, uniqueIdUser, reference, registerAmount);
    }

}