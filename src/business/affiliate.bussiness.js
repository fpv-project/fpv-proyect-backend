const {FpvBusiness} = require("./fpv.business");
const { id }  = require("../utils/dynamo/idGenerator.utils");
const { timeStamp } = require("../utils/general/timeStamp.util");
const { putItem } = require("../data/putItem.data");
const { putMoviment } = require("../data/putMoviment.data");
const { queryItems } = require("../data/queryItems.data");
const { msn } = require("../const/msn.const");

exports.AffiliateBusiness = class AffiliateBusiness extends FpvBusiness{
    constructor(typeRequest, event, tableName){
        super(typeRequest, event, tableName);
    }

    makeAggregate(ref, amount, status){
        return {
            idContributionFvp: id(), 
            reference: ref,
            amount: amount,
            statusContribution: status,
            timeRegesiter: timeStamp()
        }
    }

    newRegisterData(dynamoId, name, uniqueIdUser, nameAlias, category, status, contribution, openingDate,currencyAmount){
        return {
            idFvp: dynamoId, 
            uniqueIdUser,
            name,
            nameAlias,
            category,
            status,
            currencyAmount,
            contribution,
            openingDate
        }
    }

    async registerContributionFound(name, nameAlias, category, status, amount){

        const openingDate = timeStamp();
        const uniqueIdUser = "eqewqweq121212"; 
        const contribution = [this.makeAggregate("Aporte inicial", amount, 1)];
        const dynamoId = id();

        const dataTest = this.newRegisterData(dynamoId, name, uniqueIdUser, nameAlias, category, status, contribution, openingDate, amount);
        await putItem(dataTest, process.env.tableBankName);
       
        return dynamoId; 
    }

    validateAmountOpen(catalog, amount){
        if(amount < catalog.minumum || amount > catalog.maximum){
            const error = new Error(msn.STGYERROR);
            error.validation = [
                {
                  name: 'amount',
                  message: `${msn.GREATERTHAN} ${catalog.minumum}`,
                  value: amount
                },
                {
                    name: 'amount',
                    message: `${msn.LESSTHAN} ${catalog.minumum}`,
                    value: amount
                }
            ];
            error.statusCode = 500;
            error.statusMessage = msn.STGYERROR;
            error.time = timeStamp();
            throw error;
        }
    }

    async putMovimentAffiliate(amount, index, idUser, uniqueIdUser, reference, registerAmount){
        return putMoviment(amount, index, idUser, uniqueIdUser, reference, registerAmount);
    }

  
}