const AWS = require("aws-sdk");
const { timeStamp } = require("../utils/general/timeStamp.util");
const { msn } = require("../const/msn.const");


module.exports.cancelFpvByIdFpvAndIdUser = async (idFvp,uniqueIdUser) => {
    const paramsData = {
        TableName: `${process.env.tableBankName}`,
        Key: {
            "idFvp": idFvp,
            "uniqueIdUser": uniqueIdUser
        },
        UpdateExpression: `set #status = :r`,
        ConditionExpression: "#idFvp = :idFvp AND #uniqueIdUser = :uniqueIdUser",
        ExpressionAttributeNames:{
            '#status': "status",
            '#idFvp': "idFvp",
            '#uniqueIdUser': "uniqueIdUser"
        },
        ExpressionAttributeValues:{
            ':idFvp': idFvp,
            ':uniqueIdUser': uniqueIdUser,
            ':r': 2
        },
        ReturnValues:"UPDATED_NEW"

    };
    const dynamoDb = new AWS.DynamoDB.DocumentClient();
    return dynamoDb.update(paramsData, (err, data) => {
        if (err) {
            console.log(err);
            const error = new Error(msn.ERRORPUTCONTRIBUTION);
            error.validation = null;
            error.statusCode = 500;
            error.statusMessage = msn.ERRORPUTCONTRIBUTION;
            error.time = timeStamp();
            throw error;
        } 
    }).promise();
}