const getBody = (data) => {
    if(data.isBase64Encoded !== undefined &&  data.isBase64Encoded  === true){
        const buff = Buffer.from(data.body, "base64");
        const str = buff.toString("utf-8"); 
        return JSON.parse(str);
    }
    if(typeof data === "string" ){
        return JSON.parse(data);
    }
    return data;
}

module.exports.post = (eventRequest) => {

    return !!eventRequest ? getBody(eventRequest.body): null; 
}